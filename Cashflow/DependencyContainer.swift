//
//  DependencyContainer.swift
//  Cashflow
//
//  Created by Illia Postoienko on 9/16/19.
//  Copyright © 2019 Illia Postoienko. All rights reserved.
//

import UIKit

final class DependencyContainer {
    
    let window: UIWindow
    
    public init(
        window: UIWindow) {
        
        self.window = window
    }
}

extension DependencyContainer {
    class func configure() -> DependencyContainer {
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.backgroundColor = .white
        window.makeKeyAndVisible()
        
        return DependencyContainer(window: window)
    }
}
