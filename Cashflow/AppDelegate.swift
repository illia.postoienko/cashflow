//
//  AppDelegate.swift
//  Cashflow
//
//  Created by Illia Postoienko on 9/16/19.
//  Copyright © 2019 Illia Postoienko. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    lazy var dependency: DependencyContainer = {
        return DependencyContainer.configure()
    }()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        window = dependency.window
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {}
    func applicationDidEnterBackground(_ application: UIApplication) {}
    func applicationWillEnterForeground(_ application: UIApplication) {}
    func applicationDidBecomeActive(_ application: UIApplication) {}
    func applicationWillTerminate(_ application: UIApplication) {}
}

extension AppDelegate {
    static var dependencyContainer: DependencyContainer {
        guard let container = (UIApplication.shared.delegate as? AppDelegate)?.dependency else {
            fatalError("Cannot get dependency container from AppDelegate")
        }
        return container
    }
}
